require 'json'
require 'socket'

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]

puts "I'm #{bot_name} and connect to #{server_host}:#{server_port}"

class MySecondBot
  def initialize(server_host, server_port, bot_name, bot_key)
    tcp = TCPSocket.open(server_host, server_port)
    play(bot_name, bot_key, tcp)
  end

  private

  def play(bot_name, bot_key, tcp)
    tcp.puts join_message(bot_name, bot_key)
    react_to_messages_from_server tcp
  end

  def react_to_messages_from_server(tcp)
    while json = tcp.gets
      message = JSON.parse(json)
      msgType = message['msgType']
      msgData = message['data']
      case msgType
        when 'carPositions'
          msgData.each do | carPosition |
            if carPosition['id']['name'] == @myCarName && carPosition['id']['color'] == @myCarColor then
              piecePosition = carPosition['piecePosition']
              pieceIndex      = piecePosition['pieceIndex']
              inPieceDistance = piecePosition['inPieceDistance']
              startLaneIndex  = piecePosition['lane']['startLaneIndex']

              puts "#{pieceIndex}, #{inPieceDistance}, #{startLaneIndex}"

              if @currentPieceIndex != pieceIndex then
                @doneToChangeLaneOrNot = false
                @currentPieceIndex = pieceIndex
              end

              if @switches[pieceIndex] &&  !@doneToChangeLaneOrNot && startLaneIndex != @plans_for_lane[pieceIndex] then
                if startLaneIndex < @plans_for_lane[pieceIndex] then
                  puts "Send a switch message - Right"
                  tcp.puts switch_message("Right")
                else
                  puts "Send a switch message - Left"
                  tcp.puts switch_message("Left")
                end
                @doneToChangeLaneOrNot = true
              else
                tcp.puts throttle_message(@plans_for_throttle[pieceIndex])
              end

            end
          end

        else
          case msgType
            when 'join'
              puts 'Joined'
            when 'yourCar'
              @myCarName  = msgData['name']
              @myCarColor = msgData['color']
            when 'gameInit'
              track = msgData['race']['track']
              pieces = track['pieces']
              lanes  = track['lanes']
              puts "Track name is #{track['name']}. It has #{pieces.count} pieces and #{lanes.count} lanes"
              puts pieces
              puts lanes

              make_plans(pieces, lanes)

              @currentPieceIndex = 0
              @doneToChangeLaneOrNot = false
            when 'gameStart'
              puts 'Race started'
            when 'crash'
              puts 'Someone crashed'
            when 'gameEnd'
              puts 'Race ended'
            when 'error'
              puts "ERROR: #{msgData}"
          end
          puts "Got #{msgType}"
          tcp.puts ping_message
      end
    end
  end

  def join_message(bot_name, bot_key)
    make_msg("join", {:name => bot_name, :key => bot_key})
  end

  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

  def switch_message(right_or_left)
    make_msg("switchLane", right_or_left)
  end

  def ping_message
    make_msg("ping", {})
  end

  def make_msg(msgType, data)
    JSON.generate({:msgType => msgType, :data => data})
  end

  def make_plans(pieces, lanes)

    @switches = Array.new(pieces.count) { false }
    counter = 0
    pieces.each do | piece |
      if piece['switch'] then
        puts "switch"
        if counter != 0 then
          @switches[counter - 1] = true
        else
          @switches[pieces.count] = true
        end
      end
      
      counter += 1
    end

    puts @switches.join(',')

=begin
    @switches =  [false, false,  true, false, false, false, false,  true, false, false]
    @switches += [false, false,  true, false, false, false, false,  true, false, false]
    @switches += [false, false, false, false,  true, false, false, false,  true, false]
    @switches += [false, false, false, false,  true, false, false, false, false, false]
=end

    @plans_for_lane = Array.new()
    counter = 0
    from = counter + 1
    plan = 0
    angle_sum = 0
    pieces.each do | piece |
      if !piece['switch'] then
        if piece['angle'] then
          angle_sum += piece['angle']
        end
      else
        if angle_sum >= 90.0 then
          plan = lanes.count - 1
        elsif angle_sum <= -90.0 then
          plan = 0
        end

        i = from - 1
        j = counter - 2

        (j - i + 1).times do 
          @plans_for_lane << plan
        end
        
        from = counter
        angle_sum = 0
      end
      
      counter += 1
    end

    (pieces.count - @plans_for_lane.count).times do
      @plans_for_lane << plan
    end

    puts @plans_for_lane.join(',')

=begin
    @plans_for_lane =  [0, 0, 1, 1, 1, 1, 1, 1, 1, 1]
    @plans_for_lane += [1, 1, 0, 0, 0, 0, 0, 1, 1, 1]
    @plans_for_lane += [1, 1, 1, 1, 1, 1, 1, 1, 0, 0]
    @plans_for_lane += [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
=end

    @plans_for_throttle = Array.new(pieces.count) { 0.6 }

=begin
    @plans_for_throttle =  [0.6, 0.5, 0.5, 0.4, 0.6, 0.6, 0.8, 0.8, 0.8, 0.8]
    @plans_for_throttle += [0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6]
    @plans_for_throttle += [0.6, 0.6, 0.8, 0.8, 0.6, 0.6, 0.6, 0.6, 0.8, 0.6]
    @plans_for_throttle += [0.6, 0.6, 0.6, 0.8, 0.8, 1.0, 1.0, 1.0, 0.8, 0.8]
=end


  end

end

MySecondBot.new(server_host, server_port, bot_name, bot_key)
