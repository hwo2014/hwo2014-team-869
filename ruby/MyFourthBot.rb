require 'json'
require 'socket'

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]

puts "I'm #{bot_name} and connect to #{server_host}:#{server_port}"

class MyFourthBot
  def initialize(server_host, server_port, bot_name, bot_key)
    tcp = TCPSocket.open(server_host, server_port)
    play(bot_name, bot_key, tcp)
  end

  private

  def play(bot_name, bot_key, tcp)
    tcp.puts join_message(bot_name, bot_key)
    react_to_messages_from_server tcp
  end

  def react_to_messages_from_server(tcp)
    while json = tcp.gets
      message = JSON.parse(json)
      msgType = message['msgType']
      msgData = message['data']
      case msgType
        when 'carPositions'
          msgData.each do | carPosition |
            if carPosition['id']['name'] == @myCarName && carPosition['id']['color'] == @myCarColor then
              piecePosition = carPosition['piecePosition']
              pieceIndex      = piecePosition['pieceIndex']
              inPieceDistance = piecePosition['inPieceDistance']
              startLaneIndex  = piecePosition['lane']['startLaneIndex']
              lap = piecePosition['lap']

              lastTotalDistance = @currentTotalDistance
              @currentTotalDistance = @total_distances[pieceIndex] + inPieceDistance
              speed = @currentTotalDistance - lastTotalDistance

              totalPieceIndex = @pieces_count * lap + pieceIndex
              if lap == @laps then
                totalPieceIndex = @pieces_count * lap - 1
              end

              #puts "#{totalPieceIndex}(#{pieceIndex}), #{inPieceDistance}, #{startLaneIndex} - #{@currentTotalDistance} (SPEED: #{speed}/tick}"


              if @currentPieceIndex != pieceIndex then
                @doneToChangeLaneOrNot = false
                @currentPieceIndex = pieceIndex
              end

              if @switches[pieceIndex] &&  !@doneToChangeLaneOrNot && startLaneIndex != @plans_for_lane[pieceIndex] then
                if startLaneIndex < @plans_for_lane[pieceIndex] then
                  puts "Send a switch message - Right"
                  tcp.puts switch_message("Right")
                else
                  puts "Send a switch message - Left"
                  tcp.puts switch_message("Left")
                end
                @doneToChangeLaneOrNot = true
              else
                plan_for_throttle = @plans_for_throttle[totalPieceIndex]
                if plan_for_throttle - (speed / 10) < 0.045 then
                  case plan_for_throttle
                    when 1.0
                      plan_for_throttle = 0.5
                    when 0.9
                      plan_for_throttle = 0.4
                    when 0.8
                      plan_for_throttle = 0.3
                    when 0.7
                      plan_for_throttle = 0.2
                    when 0.6
                      plan_for_throttle = 0.1
                    when 0.5
                      plan_for_throttle = 0.0
                    when 0.4
                      plan_for_throttle = 0.0
                    when 0.3
                      plan_for_throttle = 0.0
                    when 0.2
                      plan_for_throttle = 0.0
                    when 0.1
                      plan_for_throttle = 0.0
                  end
                else
                  plan_for_throttle = 1.0
                end

                puts "#{totalPieceIndex}(#{pieceIndex}) #{@currentTotalDistance} (SPEED: #{speed}/tick} -> #{plan_for_throttle}"


                tcp.puts throttle_message(plan_for_throttle)
              end

            end
          end

        else
          case msgType
            when 'join'
              puts 'Joined'
            when 'yourCar'
              @myCarName  = msgData['name']
              @myCarColor = msgData['color']
            when 'gameInit'
              track = msgData['race']['track']
              pieces = track['pieces']
              @pieces_count = pieces.count
              lanes  = track['lanes']
              puts "Track name is #{track['name']}. It has #{pieces.count} pieces and #{lanes.count} lanes"
              puts pieces
              puts lanes

              @laps = msgData['race']['raceSession']['laps']

              make_plans(pieces, lanes, @laps)

              @currentPieceIndex = 0
              @doneToChangeLaneOrNot = false
              
              @currentTotalDistance = 0
            when 'gameStart'
              puts 'Race started'
            when 'crash'
              puts 'Someone crashed'
            when 'gameEnd'
              puts 'Race ended'
            when 'error'
              puts "ERROR: #{msgData}"
          end
          puts "Got #{msgType}"
          tcp.puts ping_message
      end
    end
  end

  def join_message(bot_name, bot_key)
    make_msg("join", {:name => bot_name, :key => bot_key})
  end

  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

  def switch_message(right_or_left)
    make_msg("switchLane", right_or_left)
  end

  def ping_message
    make_msg("ping", {})
  end

  def make_msg(msgType, data)
    JSON.generate({:msgType => msgType, :data => data})
  end

  def make_plans(pieces, lanes, laps)

    @switches = Array.new(pieces.count) { false }
    counter = 0
    pieces.each do | piece |
      if piece['switch'] then
        puts "switch"
        if counter != 0 then
          @switches[counter - 1] = true
        else
          @switches[pieces.count] = true
        end
      end
      
      counter += 1
    end

    puts @switches.join(', ')


    @plans_for_lane = Array.new()
    counter = 0
    from = counter + 1
    plan = 0
    angle_sum = 0
    pieces.each do | piece |
      if !piece['switch'] then
        if piece['angle'] then
          angle_sum += piece['angle']
        end
      else
        if angle_sum >= 90.0 then
          plan = lanes.count - 1
        elsif angle_sum <= -90.0 then
          plan = 0
        end

        i = from - 1
        j = counter - 2

        (j - i + 1).times do 
          @plans_for_lane << plan
        end
        
        from = counter
        angle_sum = 0
      end
      
      counter += 1
    end

    (pieces.count - @plans_for_lane.count).times do
      @plans_for_lane << plan
    end

    puts @plans_for_lane.join(', ')


    all_pieces = Array.new()
    laps.times do 
      all_pieces += pieces
    end

    @total_distances = Array.new()
    total_distance = 0
    @total_distances << total_distance
    all_pieces.each do | piece |
      if piece['length'] then
        length = piece['length']
      elsif piece['radius'] && piece['angle'] then
        if piece['angle'] < 0 then
          length = 2 * 3 * piece['radius'] * piece['angle'] * -1 / 360
        else
          length = 2 * 3 * piece['radius'] * piece['angle'] / 360
        end
      else
        length = 0
      end
      total_distance += length
      @total_distances << total_distance
    end


    @plans_for_throttle = Array.new()
    base_plan_curve_in  = 0.6
    base_plan_curve_out = 0.8

    plans_for_that_curve = nil
    radius = nil
    angle  = nil
    all_pieces.each do | piece |
      if piece['radius'] && piece['angle'] then
        if plans_for_that_curve == nil then
          plans_for_that_curve = Array.new()
          plans_for_that_curve << base_plan_curve_out
          radius = piece['radius']
          angle  = piece['angle']
        else
          if piece['radius'] == radius  && piece['angle'] = angle then
            last = plans_for_that_curve.last
            if base_plan_curve_in < last - 0.1 then
              case last
                when 1.0
                  plans_for_that_curve << 0.9
                when 0.9
                  plans_for_that_curve << 0.8
                when 0.8
                  plans_for_that_curve << 0.7
                when 0.7
                  plans_for_that_curve << 0.6
                when 0.6
                  plans_for_that_curve << 0.5
                when 0.5
                  plans_for_that_curve << 0.4
                when 0.4
                  plans_for_that_curve << 0.3
                when 0.3
                  plans_for_that_curve << 0.2
                when 0.2
                  plans_for_that_curve << 0.1
                when 0.1
                  plans_for_that_curve << 0.0
              end
            else
              plans_for_that_curve << base_plan_curve_in
            end
          else
            #if plans_for_that_curve.count == 2 then
            #  plans_for_that_curve[1] = base_plan_curve_in
            #end

            @plans_for_throttle += plans_for_that_curve.reverse

            plans_for_that_curve = Array.new()
            plans_for_that_curve << base_plan_curve_out
            radius = piece['radius']
            angle  = piece['angle']
          end
        end
      else
        if plans_for_that_curve != nil then
          #if plans_for_that_curve.count == 2 then
          #  plans_for_that_curve[1] = base_plan_curve_in
          #end

          @plans_for_throttle += plans_for_that_curve.reverse
    
          plans_for_that_curve = nil
          radius = nil
          angle  = nil
        end

        @plans_for_throttle << '-.-'
      end
    end

=begin
    temp_plans_for_throttle = Array.new()
    count = 0
    @plans_for_throttle.reverse.each do | plan |
      temp_plan = plan
      if temp_plan == '-.-' then
        count += 1
      else
        if temp_plan == base_plan_curve_out && count >= 4 then
          temp_plan = 0.9
        end
        count = 0
      end
      
      temp_plans_for_throttle << temp_plan
      before_temp_plan = temp_plan
    end

    @plans_for_throttle = temp_plans_for_throttle.reverse
    puts @plans_for_throttle.join(', ')
=end

    temp_plans_for_throttle = Array.new()
    before_temp_plan = 1.0
    @plans_for_throttle.reverse.each do | plan |
      temp_plan = plan
      if temp_plan == '-.-' then
        case before_temp_plan
          when 0.0
            temp_plan = 0.5
          when 0.1
            temp_plan = 0.5
          when 0.2
            temp_plan = 0.5
          when 0.3
            temp_plan = 0.5
          when 0.4
            temp_plan = 0.5
          when 0.5
            temp_plan = 0.6
          when 0.6
            temp_plan = 0.7
          when 0.7
            temp_plan = 0.8
          when 0.8
            temp_plan = 1.0
          when 0.9
            temp_plan = 1.0
          when 1.0
            temp_plan = 1.0
        end
=begin
        case before_temp_plan
          when 0.0
            temp_plan = 0.1
          when 0.1
            temp_plan = 0.2
          when 0.2
            temp_plan = 0.3
          when 0.3
            temp_plan = 0.4
          when 0.4
            temp_plan = 0.5
          when 0.5
            temp_plan = 0.6
          when 0.6
            temp_plan = 0.7
          when 0.7
            temp_plan = 0.8
          when 0.8
            temp_plan = 0.9
          when 0.9
            temp_plan = 1.0
          when 1.0
            temp_plan = 1.0
        end
=end
      end

      temp_plans_for_throttle << temp_plan
      before_temp_plan = temp_plan
    end

    @plans_for_throttle = temp_plans_for_throttle.reverse
    puts @plans_for_throttle.join(', ')

  end

end

MyFourthBot.new(server_host, server_port, bot_name, bot_key)
